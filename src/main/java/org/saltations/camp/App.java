package org.saltations.camp;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.people.v1.PeopleService;
import com.google.api.services.people.v1.PeopleServiceScopes;
import com.google.api.services.people.v1.model.ListConnectionsResponse;
import com.google.api.services.people.v1.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.Option;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;

import static java.lang.System.exit;


public class App
{
    /**
     * Key for client secrets
     */

    private static final String SECRETS_KEY = "setec-astronomy";

    private static Logger log = LoggerFactory.getLogger(App.class);

    @Option(names = { "-v", "--verbose" }, description = "Be verbose.")
    private boolean verbose = false;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "Display a help message")
    private boolean help = false;

    /**
     * Parameter for importing a client secrets JSON file.
     */

    @Option(names = {"--secrets"}, description = "Secrets file to import")
    private String secretsToImport = "";

    /**
     * Directory to store user credentials for this application.
     */

    private File datastoreFolder;

    /**
     * Global instance of the {@link FileDataStoreFactory}.
     */

    private FileDataStoreFactory datastoreFactory;

    /**
     * Global instance of the JSON factory.
     */

    private JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

    /**
     * Scopes for the services.
     */

    private Set<String> scopes = PeopleServiceScopes.all(); // CUSTOMIZE

    /**
     * Global instance of the HTTP transport.
     */

    private HttpTransport httpTransport;

    public static void main(String...args)
    {
        /*
         * Add an uncaught exception handler for reporting.
         */

        Thread.setDefaultUncaughtExceptionHandler((t,e) -> {
            e.printStackTrace();
            exit(-1);
        });


        App app = new App();

        CommandLine cli = new CommandLine(app);

        cli.parse(args);

        app.doit();
    }

    private void doit()
    {
        try {
            this.httpTransport = GoogleNetHttpTransport.newTrustedTransport();

            this.datastoreFolder = new java.io.File(System.getProperty("user.home"), ".credentials/" + App.class.getSimpleName());

            this.datastoreFactory = new FileDataStoreFactory(datastoreFolder);
        }
        catch (Throwable e)
        {
            throw new IllegalArgumentException(e);
        }


        Credential credential = authorize();

        /*
         * Build a new authorized client service.
         */

        final PeopleService service =  new PeopleService.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName(App.class.getSimpleName())
                .build();

        try {

            ListConnectionsResponse response = service.people().connections()
                    .list("people/me")
                    .setPageSize(800)
                    .setPersonFields("names")
                    .execute();

            // Print display name of connections if available.

            List<Person> connections = response.getConnections();

            connections.stream()
                    .filter(person -> person != null)
                    .filter(person -> person.getNames() != null)
                    .map(person -> person.getNames())
                    .filter(names -> names.get(0) != null)
                    .map(names -> names.get(0))
                    .filter(name -> name.getDisplayName() != null)
                    .map(name -> name.getDisplayName())
                    .sorted()
                    .forEach(name -> System.out.println(name));

        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e);
        }

    }

    /**
     * Gets authorization from Google for the service scopes.
     * <p>Does the heavy lifting of retrieving credentials, getting access tokens and storing results in a local folder for ongoing use.</p>
     *
     * @return an authorized credential object.
     */

    private Credential authorize()
    {
        importSecrets();

        /*
         * Load client secrets from the preferences.
         */

        Preferences prefs = Preferences.userNodeForPackage(App.class);

        String creds = prefs.get(SECRETS_KEY,"");

        if (creds.isEmpty()) {
            throw new IllegalArgumentException("Please import the credentials using the --secrets=XXX option.");
        }

        Credential credential = null;

        try
        {
            /*
             * Read in the bytestream from prefs into an object.
             */

            InputStream in = new ByteArrayInputStream(creds.getBytes(StandardCharsets.UTF_8));

            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(in));

            /*
             * Build flow and trigger user authorization request.
             */

            GoogleAuthorizationCodeFlow flow =
                    new GoogleAuthorizationCodeFlow.Builder(httpTransport, jsonFactory, clientSecrets, scopes)
                            .setDataStoreFactory(datastoreFactory)
                            .setAccessType("offline")
                            .build();

            credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");

            log.debug("Credentials saved to {}", datastoreFolder.getAbsolutePath());
        }
        catch (Throwable e)
        {
            throw new IllegalArgumentException(e);
        }

        return credential;
    }

    /**
     * Imports client secret json file and stores it in the user preferences.
     */

    private void importSecrets()
    {
        /*
         * If secret should be imported, import it.
         */

        if (!secretsToImport.isEmpty())
        {
            if (!new File(secretsToImport).exists()) {
                throw new IllegalArgumentException(String.format("File <%s> cannot be imported, it does not exist", secretsToImport));
            }

            Preferences prefs = Preferences.userNodeForPackage(App.class);

            try {
                String content = new String(Files.readAllBytes(Paths.get(secretsToImport)));

                prefs.put(SECRETS_KEY, content);
            }
            catch (Throwable e)
            {
                throw new IllegalArgumentException(e);
            }
        }
    }

}
